<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
	public function Login(){
		if(Auth::attempt(Request::only('email','password'))){
			return Auth::user();
		} else {
			return 'Invalid e-mail/password combination.';
		}
	}

	public function Logout(){
		Auth::logout();
		return 'Logged out.';
	}
}