<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Post extends Eloquent {
	/**
	 * The database table used by the model.CRUD Service
	 *
	 * @var string
	 *
	 */

	protected $table = 'posts';
}