<?php 
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder {

    public function run() {
    	DB::table('users')->delete();
        
        User::create([
            'first_name' => 'Inna',
            'last_name' => 'Sulg',
            'email' => 'innasulg@marionkool.org',
            'password' => Hash::make('anexcellentpassword'),
            'user_type' => 'admin',
            'user_description' => '""',
            'user_classes' => '""',
            'user_image' => '""'
        ]);

    }

}