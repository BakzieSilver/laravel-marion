<?php 
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Post;

class PostTableSeeder extends Seeder {
	public function run(){
		DB::table('posts')->delete();

		Post::create([
			"post_author" => "test",
			"post_title" => "A test post",
			"post_contents" => "The contents of this post are exciting",
			"post_type" => "News",
			"user_id" => 0,
			"page_id" => 0
		]);
	}
}

?>