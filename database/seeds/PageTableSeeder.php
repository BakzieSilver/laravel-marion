<?php 
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PageTableSeeder extends Seeder {
	DB::table('pages')->delete();

	User::create([
		[
		'first_name' => 'JC',
		'last_name' => 'Denton',
		'email' => 'ss@bakzie.com',
		'password' => 'bionicman1',
		'user_type' => 'sudo',
		'user_description' => '""',
		'user_classes' => '""',
		'user_image' => '""'
		],
		[
		'first_name' => 'Inna',
		'last_name' => 'Sulg',
		'email' => 'ss@bakzie.com',
		'password' => 'anexcellentpassword',
		'user_type' => 'admin',
		'user_description' => '""',
		'user_classes' => '""',
		'user_image' => '""'
		]
	]);
}

?>